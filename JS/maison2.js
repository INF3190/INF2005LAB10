/**
 * @author Johnny Tsheke
 */
$(document).ready(function(){
	//dessine maison
	var can=$("#canvas1")[0];
   var contxt=can.getContext("2d");// !tableau avec jQuery
   var larg=can.width;
   var haut=can.height;
	//le toit
	contxt.moveTo(100,150);
	contxt.lineTo(500,150);
	contxt.lineTo(300,50);
	contxt.lineTo(100,150);
	contxt.fillStyle = 'green';
	contxt.fill();//rempli la figure
	contxt.stroke();//dessine les bordure
	//la maison --les murs
	contxt.beginPath(); //sert a commencer un nouveau chemin
	contxt.moveTo(100,150);
	contxt.rect(100,150,400,300);
	contxt.fillStyle = "gray";
	contxt.fillRect(100,150,400,300);
	contxt.stroke();
	//rectangle rouge
	contxt.beginPath();
	contxt.moveTo(250,250);
	contxt.rect(250,250,100,200);
	contxt.fillStyle = "red";
	contxt.fillRect(250,250,100,200);
	contxt.stroke();
	//carré blanc
	contxt.beginPath();
	contxt.moveTo(120,250);
	contxt.rect(120,250,100,70);
	contxt.fillStyle = "white";
	contxt.fillRect(120,250,100,70);
	contxt.stroke();
	 var x=50;
	 var y=50;
	 var rayon=20;
	 //dessiner le soleil
	 contxt.beginPath();
	 contxt.arc(x,y,rayon,0,Math.PI*2);
	 contxt.fillStyle = "yellow";
	 contxt.strokeStyle='Fuchsia';// couleur des lignes
	 contxt.fill();
	 contxt.stroke();
	 //dessin rayons solaires
	 
	 for(i=0;i<5;i++)
	{
	contxt.moveTo(x,y);
	dis=i*10;
	max=5*10;
	xray=x+70+max-dis;
	yray=y+40+dis;
	contxt.lineTo(xray,yray);
	//contxt.lineTo(x+60,y+50);	
	}
	contxt.stroke();
	contxt.strokeStyle='black';// on remet la couleur par defaut
	 //écriture du texte
	 contxt.beginPath(); 
	 contxt.font="30px Arial";
	 //contxt.rotate(90*Math.PI/180);
	 //contxt.strokeText("Maison écologique",150,-550);
	 //décommentes les 2 ligne précédente et 
	 //commenter la ligne suivante pour faire tourner le texte sur le bord droit
	 contxt.strokeText("Maison écologique",340,480);//text et point de début
	
});
