/**
 * @author Johnny Tsheke
 */
$(document).ready(function(){
	//dessine maison
	var can=$("#canvas1")[0];
	//can.css({"width":"600px","height":"500px"});
   var contxt=can.getContext("2d");// !tableau avec jQuery
   var larg=can.width;
   var haut=can.height;
	//contxt.moveTo(100,450);
	
	contxt.moveTo(500,150);
	contxt.lineTo(500,450);
	contxt.lineTo(100,450);
	contxt.lineTo(100,150);
	contxt.lineTo(500,150);
	contxt.lineTo(300,50);
	contxt.lineTo(100,150);
	
	var x=50;
	var y=50;
	var rayon=20;
	contxt.moveTo(x,y);
	
	contxt.arc(x,y,rayon,0,Math.PI*2);
	contxt.fillStyle = 'green';
      contxt.fill();
	contxt.stroke();
	contxt.fillStyle = "gray";
      contxt.fillRect(100,150,400,300);
      contxt.fillStyle = "white";
       contxt.fillRect(120,250,100,70);
       contxt.fillStyle = "red";
       contxt.fillRect(250,250,100,200);
	
});
